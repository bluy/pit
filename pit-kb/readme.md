# 接口
## 检索

http://127.0.0.1:8000/admin/searcher?q=你


## 添加[POST]

http://127.0.0.1:8000/admin/wiki
    
```
{
"title": "你1",
"content": "好1",  
"topics":"你那么1",
"source": "KPI那么1"
}
```
## 删除 [DELETE]

http://127.0.0.1:8000/admin/wiki/1793613961202188290

## 修改 [PUT]
http://127.0.0.1:8000/admin/wiki/1793611664728408065
```shell
{
"title": "你好2",
"content": "你2",  
"topics":"你现在KPI2",
"source": "你现在KPI2"
}
```
