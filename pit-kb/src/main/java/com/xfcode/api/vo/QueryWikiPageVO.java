package com.xfcode.api.vo;

import com.xfcode.api.page.DatePage;
import lombok.Data;

@Data
public class QueryWikiPageVO  extends DatePage {
    private String title;
    private String content;

    private String source;
    private String createBy;
    private String indexNum;
    private String topics;
    private String keywords;
}
