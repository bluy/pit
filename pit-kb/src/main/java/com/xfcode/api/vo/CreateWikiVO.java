package com.xfcode.api.vo;

import lombok.Data;

@Data
public class CreateWikiVO {
    private String title;
    private String content;

    private String source;
    private String createBy;
    private String indexNum;
    private String topics;
    private String keywords;
}
