package com.xfcode.api;

import lombok.Data;

/**
 * @Author guanxf
 * @Date 2022/05/21 12:01
 * @Description:
 * @Version 1.1
 */
@Data
public class WikiTree {
    /**主键*/
    private String id;
    /**父级节点*/
    private String pid;
    /**类型名称*/
    private String name;
    /**类型编码*/
    private String code;
}
