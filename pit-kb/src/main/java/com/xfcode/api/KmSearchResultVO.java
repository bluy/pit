package com.xfcode.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;
import java.io.Serializable;
import java.util.List;

@Data
public class KmSearchResultVO<T> implements Serializable {
    private boolean success;
    private List<String>  paramPath;
    private IPage<T> page;
}
