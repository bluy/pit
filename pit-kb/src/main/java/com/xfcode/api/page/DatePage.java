package com.xfcode.api.page;

import lombok.Data;

@Data
public class DatePage<T> extends CleanPage<T> {

    private String beginTime;

    private String endTime;

}
