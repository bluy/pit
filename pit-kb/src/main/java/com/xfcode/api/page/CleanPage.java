package com.xfcode.api.page;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import java.util.List;

@Data
public class CleanPage<T> extends Page<T> {
    private  Long pageNum;
    private  Long pageSize;

    public void setPageNum(Long pageNum) {
        this.pageNum = pageNum;
        this.current=pageNum;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
        this.size=pageSize;
    }

    @Override
    public List getRecords() {
        return super.getRecords();
    }

    @Override
    public long offset() {
        return super.offset();
    }



    @Override
    public long getTotal() {
        return super.getTotal();
    }
}
