package com.xfcode.api;

import lombok.Data;
import java.io.Serializable;

@Data
public class WikiDTO implements Serializable {
    private String keyword;
    private String[] keywords;
	private String title;
	private String content;
	private String[] topics;
    private String source;
    private String createBy;
    private String createTime;
}
