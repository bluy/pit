package com.xfcode.api;

import com.xfcode.api.page.CleanPage;
import lombok.Data;
import lombok.ToString;
import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class QueryWikiPageVO extends CleanPage implements Serializable {
    private String q;
    //高级检索标识
    private Integer advanceSearch;

    private Integer withinSearch;

    private String title;

    private String fileNo;

    private List<String> category;

    private List<String> source;

    private String publicBeginTime;

    private String publicEndTime;

    private List<String> versions;

    private List<String> businessTypes;

    private List<String> keywords;

	private List<String> topics;

	private String content;

	//结果排序字段
	private String column;
	private String order;
}
