package com.xfcode.controller;

import com.xfcode.api.vo.CreateWikiVO;
import com.xfcode.api.vo.QueryWikiPageVO;
import com.xfcode.common.core.controller.BaseController;
import com.xfcode.common.core.domain.AjaxResult;
import com.xfcode.common.core.page.TableDataInfo;
import com.xfcode.domain.Wiki;
import com.xfcode.api.page.PageTable;
import com.xfcode.service.WikiService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;

@RestController
@RequestMapping("/wiki")
@Slf4j
public class WikiController extends BaseController {
	@Autowired
	private WikiService wikiService;
	/**
	 * 分页列表查询
	 * @return
	 */
	@ApiOperation(value="分页列表查询")
	@GetMapping
	public TableDataInfo<?> queryPageList(QueryWikiPageVO page) {
		Page<Wiki> pageList = wikiService.getPage(page);
		return  PageTable.getPageTable(pageList);
	}

	/**
	 *   添加
	 *
	 * @param createWikiVO
	 * @return
	 */
	@ApiOperation(value="添加")
	@PostMapping
	public AjaxResult add(@RequestBody CreateWikiVO createWikiVO) {
		wikiService.save(createWikiVO);
		return success();
	}

	/**
	 *  编辑
	 * @param createWikiVO
	 * @return
	 */
	@ApiOperation(value="编辑")
	@PutMapping("/{id}")
	public AjaxResult edit(@PathVariable("id") Long id,@RequestBody CreateWikiVO createWikiVO) {
		wikiService.updateById(id,createWikiVO);
		return success();
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@ApiOperation(value="通过id删除")
	@DeleteMapping("/{id}")
	public AjaxResult delete(@PathVariable("id") Long id) {
		wikiService.deleteById(id);
		return success();
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@ApiOperation(value="批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public AjaxResult deleteBatch(@RequestParam(name="ids") String ids) {
		this.wikiService.removeByIds(Arrays.asList(ids.split(",")));
		return success();
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@ApiOperation(value="通过id查询")
	@GetMapping(value = "/queryById")
	public AjaxResult queryById(@RequestParam(name="id",required=true) String id) {
		Wiki wiki = wikiService.getById(id);
		return AjaxResult.success(wiki);
	}
}
