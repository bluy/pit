package com.xfcode.controller;

import java.util.List;

import com.xfcode.common.core.controller.BaseController;
import com.xfcode.common.core.domain.AjaxResult;
import com.xfcode.common.core.page.TableDataInfo;
import com.xfcode.api.QueryWikiPageVO;
import com.xfcode.api.KmSearchResultVO;
import com.xfcode.service.SearcherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/searcher")
@Slf4j
public class SearcherController extends BaseController {
	@Autowired
	private SearcherService kmSearchRecordService;

	@ApiOperation(value="普通检索")
	@GetMapping
	public AjaxResult search(QueryWikiPageVO queryPage){
		queryPage.setAdvanceSearch(1);
		queryPage.setTitle(queryPage.getQ());
		KmSearchResultVO kmSearchResultVO = kmSearchRecordService.search(queryPage);
		return AjaxResult.success(kmSearchResultVO);
	}


	/**
	 * @param queryPage
	 * @功能描述 传入指定的indexid，列出相似的文档
	 */
	@ApiOperation(value="排重检索")
	@GetMapping(value = "/distinct")
	public AjaxResult searchDistinct(QueryWikiPageVO queryPage)  {
			queryPage.setColumn("_score");
			queryPage.setOrder("desc");
			KmSearchResultVO kmSearchResultVO = kmSearchRecordService.searchDistinct(queryPage);
			return AjaxResult.success(kmSearchResultVO);
	}


	@ApiOperation(value="-热词报告")
	@GetMapping(value = "/hotKeywordReport")
	public TableDataInfo<?> hotKeywordReport(){
		List<String> result =  kmSearchRecordService.searchHotKeyword();
		return getDataTable(result);
	}

	@ApiOperation(value="-热门专题报告")
	@GetMapping(value = "/hotTopicReport")
	public TableDataInfo<?> hotTopicReport(){
		List<String> result =  kmSearchRecordService.searchHotTopic();
		return getDataTable(result);
	}

}
