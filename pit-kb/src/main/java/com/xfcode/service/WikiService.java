package com.xfcode.service;

import com.xfcode.api.vo.CreateWikiVO;
import com.xfcode.api.vo.QueryWikiPageVO;
import com.xfcode.domain.Wiki;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

public interface WikiService extends  IService<Wiki>{
    void save(CreateWikiVO createWikiVO);

    Page<Wiki> getPage(QueryWikiPageVO page);

    void updateById(Long id, CreateWikiVO createWikiVO);

    void deleteById(Long id);
}
