package com.xfcode.service;

import com.xfcode.api.QueryWikiPageVO;
import com.xfcode.api.KmSearchResultVO;
import com.xfcode.api.WikiDTO;
import com.xfcode.api.WikiTree;
import java.util.List;

public interface SearcherService {
    KmSearchResultVO search(QueryWikiPageVO queryPage);

    List<String> searchHotKeyword();

    List<String> searchHotTopic() ;

    List<WikiTree> searchHotTopicTree() ;

    KmSearchResultVO searchDistinct(QueryWikiPageVO queryPage);

    WikiDTO searchByProperty(String property,String value);

}
