package com.xfcode.service;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class SearcherBaseService {
    public abstract String getEsAliasName();

    public abstract String getEsAlias();
    //秒
    public static Integer SearchTimeOutSeconds = 10;
    //1分钟
    public static Integer SaveTimeOutMinutes = 1;
    public static String SearchTermSplit = " + ";
    //1小时
    public static Integer SaveTimeOutHours = 1;


    public float getContentSearchBoost() {
        return 1;
    }

    public float getKeywordSearchBoost() {
        return 1;
    }

    public float getTitleSearchBoost() {
        return 1;
    }

    public static String formatDateyyyyMM(Date date){
        SimpleDateFormat sd = new SimpleDateFormat("yyyyMM");
        return sd.format(date);
    }
    public String getBuildIndex() {
        String indexSuffix =formatDateyyyyMM(new Date());
        return getEsAlias() + "_" + indexSuffix;
    }

}
