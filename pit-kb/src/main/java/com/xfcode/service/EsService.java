package com.xfcode.service;
import com.xfcode.api.vo.CreateWikiVO;

public interface EsService {
    String saveToEs(CreateWikiVO createWikiVO);

    void initWikiTemplate();

    void updateById(String indexId, CreateWikiVO createWikiVO);

    void deleteById(String indexId);

}
