package com.xfcode.service.impl;

import com.xfcode.api.*;
import com.xfcode.api.vo.CreateWikiVO;
import com.xfcode.domain.Wiki;
import com.xfcode.service.SearcherBaseService;
import com.xfcode.service.SearcherService;
import com.xfcode.utils.KmStringUtils;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class SearcherServiceImpl extends SearcherBaseService implements SearcherService {
    private  String LogKbWikiAliasName = "log_prod_kb_wiki_alias";
    private  String LogKbWikiAlias = "log_prod_kb_wiki";
    @Override
    public String getEsAliasName() {
        return  LogKbWikiAliasName;
    }

    @Override
    public String getEsAlias() {
        return LogKbWikiAlias;
    }
    @Autowired
    private RestHighLevelClient restHighLevelClient;
    @Override
    public KmSearchResultVO search(QueryWikiPageVO queryWikiPage)  {
        SearchSourceBuilder searchSourceBuilder = buildSearchSort(queryWikiPage);
        QueryBuilder boolQueryBuilder=buildQuery(queryWikiPage);
        searchSourceBuilder.query(boolQueryBuilder);

        // highlight field 仅对title
        HighlightBuilder highlightBuilder = new HighlightBuilder().field("title").requireFieldMatch(false);
        highlightBuilder.preTags("<span style=\"color:red\">");
        highlightBuilder.postTags("</span>");
        searchSourceBuilder.highlighter(highlightBuilder);


        //分页，注意分页的坑，from要从0开始
        long from = queryWikiPage.getCurrent() < 1 ? 0 : queryWikiPage.getSize() *(queryWikiPage.getCurrent()- 1) ;
        long size = queryWikiPage.getSize() > 100 ? 100 : queryWikiPage.getSize();
        size = size < 0 ? 10 : size;
        searchSourceBuilder.from((int) from);
        searchSourceBuilder.size((int) size);

        //超时 60S
        searchSourceBuilder.timeout(new TimeValue(SearchTimeOutSeconds, TimeUnit.SECONDS));

        // 过滤返回结果字段，去掉非必要信息，关键：去掉content
        //{"content","keywords"};
        String excludeFields[] = {"content"};
        //{"id","title","source","versions","pubTime"};
        String includeFields[] = {};
        searchSourceBuilder.fetchSource(includeFields,excludeFields);
        return queryEs(queryWikiPage, searchSourceBuilder);
    }


    /*
   管理排重检索
    */
    @Override
    public KmSearchResultVO searchDistinct(QueryWikiPageVO queryWikiPageVO)  {
        Wiki wiki = new Wiki();
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        List<String> keywords = new ArrayList<>();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();

        String duplicateCheckHitRate =  "50%";
        //1、标题检索 高级用must，高级用should
        if (queryWikiPageVO.getTitle() != null && !queryWikiPageVO.getTitle().isEmpty()) {
            keywords.add(queryWikiPageVO.getTitle());
            wiki.setTitle(queryWikiPageVO.getTitle());
            boolQueryBuilder
                    .should()
                    .add(QueryBuilders.matchQuery("title", queryWikiPageVO.getTitle()).minimumShouldMatch(duplicateCheckHitRate)
                            .analyzer("ik_smart"));

        }

        //2、全文检索  高级用must，高级用should
        if (queryWikiPageVO.getContent() != null && !queryWikiPageVO.getContent().isEmpty()) {
            keywords.add(queryWikiPageVO.getContent());
            wiki.setContent(queryWikiPageVO.getContent());
            boolQueryBuilder
                    .should()
                    .add(QueryBuilders.matchQuery("content", queryWikiPageVO.getContent()).minimumShouldMatch(duplicateCheckHitRate)
                            .analyzer("ik_smart"));

        }

        //排序，对字典文本字段，去掉后缀
        if(queryWikiPageVO.getColumn() != null
                && !queryWikiPageVO.getColumn().isEmpty()
                && queryWikiPageVO.getOrder() != null
                && !queryWikiPageVO.getOrder().isEmpty()) {
            String column = queryWikiPageVO.getColumn();
            String order = queryWikiPageVO.getOrder();
            if(column.endsWith("_dictText")) {
                column = column.substring(0, column.lastIndexOf("_dictText"));
            }


            FieldSortBuilder fieldSortBuilder = SortBuilders.fieldSort(column).order(SortOrder.fromString(order));
            searchSourceBuilder.sort(fieldSortBuilder);
        }

        searchSourceBuilder.query(boolQueryBuilder);

        // highlight field 仅对title
        if(queryWikiPageVO.getTitle() != null && !queryWikiPageVO.getTitle().isEmpty()) {
            HighlightBuilder highlightBuilder = new HighlightBuilder().field("title").requireFieldMatch(false);
            highlightBuilder.preTags("<span style=\"color:blue\">");
            highlightBuilder.postTags("</span>");
            searchSourceBuilder.highlighter(highlightBuilder);
        }

        //注意分页的坑，from要从0开始
        long from = queryWikiPageVO.getCurrent() < 1 ? 0 : queryWikiPageVO.getSize() *(queryWikiPageVO.getCurrent()- 1) ;
        long size = queryWikiPageVO.getSize() > 100 ? 100 : queryWikiPageVO.getSize();
        size = size < 0 ? 10 : size;
        searchSourceBuilder.from((int) from);
        searchSourceBuilder.size((int) size);

        //超时 60S
        searchSourceBuilder.timeout(new TimeValue(SearchTimeOutSeconds, TimeUnit.SECONDS));

        // 过滤返回结果字段，去掉非必要信息，关键：去掉content
        //{"content","keywords"};
        String excludeFields[] = {"content"};
        String includeFields[] = {};
        searchSourceBuilder.fetchSource(includeFields,excludeFields);
         return  queryEs(queryWikiPageVO,searchSourceBuilder);
    }

    @Override
    public WikiDTO searchByProperty(String property,String value){
        if (ObjectUtils.isEmpty(value)){
            return null ;
        }
        try {
            SearchResponse searchResponse = getSearchByDocIdResponse(property,value);
            if(searchResponse.status() != RestStatus.OK){
                return  null;
            }
            else{
                long c = searchResponse.getHits().getTotalHits().value;
                if(c == 0){
                    return null;
                }
                else{
                    //返回ES记录
                    SearchHits hits = searchResponse.getHits();
                    SearchHit[] searchHits = hits.getHits();
                    SearchHit hit = searchHits[0];
                    WikiDTO uniqueWikiVO = JSON.parseObject(hit.getSourceAsString(), WikiDTO.class);
                    return uniqueWikiVO;
                }
            }
        }
        catch (Exception e){
            return null;
        }
    }

    @Override
    public List<String> searchHotKeyword() {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        TermsAggregationBuilder aggregationBuilder =
                AggregationBuilders
                        .terms("keywords")
                        .field("keywordsMax")
                        .size(10);

        searchSourceBuilder.aggregation(aggregationBuilder);
//
        //超时 60S
        searchSourceBuilder.timeout(new TimeValue(SearchTimeOutSeconds, TimeUnit.SECONDS));

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(LogKbWikiAliasName);


        List<String> result = new ArrayList<>();
        try {
            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            if(searchResponse.status() != RestStatus.OK || searchResponse.getHits().getTotalHits().value<=0){
                return null;
            }
            else {
                Aggregations responseAggregations = searchResponse.getAggregations();
                ParsedStringTerms terms = responseAggregations.get("keyword");
                List<? extends Terms.Bucket> buckets = terms.getBuckets();

                for (Terms.Bucket bucket : buckets) {
                    String term = bucket.getKeyAsString();
                    //Integer docCount = new Long( bucket.getDocCount()).intValue();
                    result.add(term);
                }
            }
        } catch (IOException e) {
            log.warn("es查询失败",e);
        }
        return  result;
    }

    public List<WikiTree> searchHotTopicTree(){
        List<WikiTree> result = new ArrayList<>();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        TermsAggregationBuilder aggregationBuilder =
                AggregationBuilders
                        .terms("title")
                        .field("titleMax")
                        .size(10);

        searchSourceBuilder.aggregation(aggregationBuilder);
        //超时 60S
        searchSourceBuilder.timeout(new TimeValue(SearchTimeOutSeconds, TimeUnit.SECONDS));

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(LogKbWikiAliasName);

        SearchResponse searchResponse = null;
        try {
            searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.warn("es查询失败",e);
        }
        if(searchResponse.status() != RestStatus.OK || searchResponse.getHits().getTotalHits().value<=0){
            return null;
        }
        else {
            Aggregations responseAggregations = searchResponse.getAggregations();
            ParsedStringTerms terms = responseAggregations.get("title");
            List<? extends Terms.Bucket> buckets = terms.getBuckets();

            for (Terms.Bucket bucket : buckets) {
                String term = bucket.getKeyAsString();
                WikiTree wikiTree = new WikiTree();
                wikiTree.setId("");
                wikiTree.setPid("");
                wikiTree.setName(term);
                wikiTree.setCode("");
                if(wikiTree !=null ){
                    result.add(wikiTree);
                }
            }
        }
        return  result;
    }


    @Override
    public List<String> searchHotTopic()  {
        List<String> result = new ArrayList<>();
        List<WikiTree> wikiTrees = searchHotTopicTree();
        if(wikiTrees != null && !wikiTrees.isEmpty()) {
            wikiTrees
                    .stream()
                    .forEach(
                            e -> {
                                result.add(e.getName());
                            });
            return result;
        }
        return result;
    }


    private SearchResponse getSearchByDocIdResponse(String  property,String value) throws IOException {
        //通过id查询
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        QueryBuilder queryBuilder = QueryBuilders.termsQuery(property, value);//.idsQuery().addIds(indexId);
        searchSourceBuilder.query(queryBuilder);
        //超时 10S
        searchSourceBuilder.timeout(new TimeValue(SearchTimeOutSeconds, TimeUnit.SECONDS));
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(LogKbWikiAliasName);

        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        return searchResponse;
    }


    private KmSearchResultVO queryEs(QueryWikiPageVO queryWikiPage,SearchSourceBuilder searchSourceBuilder) {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        searchRequest.indices(LogKbWikiAliasName);
        log.debug(searchRequest.source().toString());

        KmSearchResultVO kmSearchResultVO = new KmSearchResultVO();
        try {

            List<String> paramPaths = new ArrayList<>();
            kmSearchResultVO.setParamPath(paramPaths);

            SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            if(searchResponse.status() != RestStatus.OK || searchResponse.getHits().getTotalHits().value<=0){
                kmSearchResultVO.setPage(queryWikiPage);
                kmSearchResultVO.setSuccess(true);
            }
            else {
                List<WikiDTO> uniqueWikiVOList = new ArrayList<>();
                SearchHits hits = searchResponse.getHits();
                SearchHit[] searchHits = hits.getHits();
                for (SearchHit hit : searchHits) {
                    log.debug(hit.getSourceAsString());
                    WikiDTO uniqueWikiVO = JSON.parseObject(hit.getSourceAsString(), WikiDTO.class);
                    Map<String, HighlightField> highlightFields = hit.getHighlightFields();
                    //获取title高亮显示
                    if (highlightFields != null && highlightFields.size() > 0) {
                        HighlightField highlight = highlightFields.get("title");
                        //获取高亮显示的字段
                        Text[] fragments = highlight.fragments();
                        String fragmentString = fragments[0].string();

                        uniqueWikiVO.setTitle(fragmentString);
                    }
                    uniqueWikiVOList.add(uniqueWikiVO);

                }

                queryWikiPage.setRecords(uniqueWikiVOList);
                //set page
                queryWikiPage.setTotal(hits.getTotalHits().value);
                kmSearchResultVO.setPage(queryWikiPage);
                kmSearchResultVO.setSuccess(true);
            }
        } catch (IOException e) {
            log.warn("es查询失败",e);
        }
        return    kmSearchResultVO ;
    }

    private SearchSourceBuilder buildSearchSort(QueryWikiPageVO queryWikiPage) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        //排序，对字典文本字段，去掉后缀
        if(queryWikiPage.getColumn() != null
                && !queryWikiPage.getColumn().isEmpty()
                && queryWikiPage.getOrder() != null
                && !queryWikiPage.getOrder().isEmpty()) {
            String column = queryWikiPage.getColumn();
            String order = queryWikiPage.getOrder();
            if(column.endsWith("_dictText")) {
                column = column.substring(0, column.lastIndexOf("_dictText"));
            }

            FieldSortBuilder fieldSortBuilder = SortBuilders.fieldSort(column).order(SortOrder.fromString(order));
            searchSourceBuilder.sort(fieldSortBuilder);
        }
        return searchSourceBuilder;
    }

    private BoolQueryBuilder buildQuery(QueryWikiPageVO queryWikiPageVO) {
        //最终条件
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //普通检索的条件，综合 ： 标题、关键字、全文 合并检索
        BoolQueryBuilder boolQueryBuilderDefault = QueryBuilders.boolQuery();
        CreateWikiVO wiki=new CreateWikiVO();
        List<String> keywords = new ArrayList<>();
        String paramPath = "";
        //1、分类为必需条件 用filter
        if (queryWikiPageVO.getCategory() != null) {
            paramPath = SearchTermSplit + "分类: ";
            List<String> categorys = queryWikiPageVO.getCategory();
            for (int i = 0; i < categorys.size(); i++) {
//                paramPath = paramPath.concat(dictUtils.getDicText("km_dict_category", categorys.get(i)));
                paramPath = paramPath.concat(categorys.get(i));
                paramPath = paramPath.concat(",");
            }
            paramPath = paramPath.substring(0, paramPath.length() - 1);
            boolQueryBuilder
                    .filter()
                    .add(QueryBuilders.termsQuery("category", categorys));
        }

        //2、标题检索 高级用must，高级用should
        if (queryWikiPageVO.getTitle() != null && !queryWikiPageVO.getTitle().isEmpty()) {
            paramPath = SearchTermSplit + "标题: " + queryWikiPageVO.getTitle() +  paramPath;
            keywords.add(queryWikiPageVO.getTitle());
            wiki.setTitle(queryWikiPageVO.getTitle());
            if(queryWikiPageVO.getAdvanceSearch() != null && queryWikiPageVO.getAdvanceSearch() == 1) {
                boolQueryBuilder
                        .must()
                        .add(QueryBuilders.matchQuery("title", queryWikiPageVO.getTitle())
                                .analyzer("ik_smart").boost(getTitleSearchBoost()));
            }
            else{
                boolQueryBuilderDefault
                        .should()
                        .add(QueryBuilders.matchQuery("title", queryWikiPageVO.getTitle())
                                .analyzer("ik_smart").boost(getTitleSearchBoost()));
            }
        }
        //3、关键字检索 用term精确匹配;  高级用must，高级用should
        String keywordString = "";
        if (queryWikiPageVO.getKeywords() != null && queryWikiPageVO.getKeywords().size() > 0) {
            keywordString = String.join(",", queryWikiPageVO.getKeywords() );
            if((queryWikiPageVO.getTitle() != null && !queryWikiPageVO.getTitle().isEmpty()
                    && keywordString.equals(queryWikiPageVO.getTitle()))
                    || queryWikiPageVO.getAdvanceSearch()!=1){
                paramPath = SearchTermSplit + "关键字" + paramPath;
            }
            else{
                paramPath = SearchTermSplit + "关键字: " + keywordString + paramPath ;
            }

            keywords.addAll(queryWikiPageVO.getKeywords().subList(0, queryWikiPageVO.getKeywords().size()));
            wiki.setKeywords(KmStringUtils.concatListToString(queryWikiPageVO.getKeywords()));
            BoolQueryBuilder boolQueryBuilderKeywords = QueryBuilders.boolQuery();
            queryWikiPageVO.getKeywords().forEach(e -> {
                boolQueryBuilderKeywords
                        .should()
                        .add(QueryBuilders.termsQuery("keywords", e)
                                .boost(getKeywordSearchBoost()));
            });
            if(queryWikiPageVO.getAdvanceSearch() != null && queryWikiPageVO.getAdvanceSearch() == 1) {
                boolQueryBuilder
                        .must()
                        .add(boolQueryBuilderKeywords);
            }
            else{
                boolQueryBuilderDefault
                        .should()
                        .add(boolQueryBuilderKeywords);
            }
        }
        //4、全文检索  高级用must，高级用should
        if (queryWikiPageVO.getContent() != null && !queryWikiPageVO.getContent().isEmpty()) {
            if((keywordString != null && !keywordString.isEmpty()
                    && keywordString.equals(queryWikiPageVO.getContent()))
                    ||  queryWikiPageVO.getAdvanceSearch()!=1){
                paramPath = SearchTermSplit + "全文" + paramPath;
            }
            else {
                paramPath = SearchTermSplit + "全文: " + queryWikiPageVO.getContent() + paramPath ;
            }
            keywords.add(queryWikiPageVO.getContent());
            wiki.setContent(queryWikiPageVO.getContent());
            if(queryWikiPageVO.getAdvanceSearch() != null && queryWikiPageVO.getAdvanceSearch() == 1) {
                boolQueryBuilder
                        .must()
                        .add(QueryBuilders.matchQuery("content", queryWikiPageVO.getContent())
                                .analyzer("ik_smart").boost(getContentSearchBoost()));
            }
            else {
                boolQueryBuilderDefault
                        .should()
                        .add(QueryBuilders.matchQuery("content", queryWikiPageVO.getContent())
                                .analyzer("ik_smart").boost(getContentSearchBoost()));
            }
        }

        //处理普通检索的合并条件：标题、关键字、全文
        if(queryWikiPageVO.getAdvanceSearch() == null || queryWikiPageVO.getAdvanceSearch() == 0) {
            boolQueryBuilder
                    .must()
                    .add(boolQueryBuilderDefault);
        }


        //6、来源检索 用filter
        if (queryWikiPageVO.getSource() != null && !queryWikiPageVO.getSource().isEmpty()) {
            String tmpSource ="";
            List<String> source = queryWikiPageVO.getSource();
            for (int i = 0; i < source.size(); i++) {
//                tmpSource = tmpSource.concat(dictUtils.getDicText("km_dict_source", source.get(i)));
                tmpSource=source.get(i);
                tmpSource = tmpSource.concat(",");
            }
            paramPath =  SearchTermSplit + "来源: " + tmpSource.substring(0, tmpSource.length() - 1) +  paramPath ;
            boolQueryBuilder
                    .filter()
                    .add(QueryBuilders.termsQuery("source", source));
        }

        //7、业务类型检索（多选） 用filter
        if (queryWikiPageVO.getBusinessTypes() != null && queryWikiPageVO.getBusinessTypes().size() > 0) {
            String tmpBusinessType = "";
            List<String> businessTypes = queryWikiPageVO.getBusinessTypes();
            for (int i = 0; i < businessTypes.size(); i++) {
//                tmpBusinessType = tmpBusinessType.concat(dictUtils.getDicText("km_dict_business", businessTypes.get(i)));
                tmpBusinessType =businessTypes.get(i);
                tmpBusinessType = tmpBusinessType.concat(",");
            }
            paramPath =  SearchTermSplit + "业务类型: " + tmpBusinessType.substring(0, tmpBusinessType.length() - 1) + paramPath  ;
            boolQueryBuilder
                    .filter()
                    .add(QueryBuilders.termsQuery("businessTypes", businessTypes));
        }

        //8、版本状态检索（多选） 用filter
        if (queryWikiPageVO.getVersions() != null && queryWikiPageVO.getVersions().size() > 0) {
            String tmpVersion = "";
            List<String> versions = queryWikiPageVO.getVersions();
            for (int i = 0; i < versions.size(); i++) {
//                tmpVersion = tmpVersion.concat(dictUtils.getDicText("km_dict_versions", versions.get(i)));
                tmpVersion = versions.get(i);
                tmpVersion = tmpVersion.concat(",");
            }
            paramPath = SearchTermSplit + "版本: " +  tmpVersion.substring(0, tmpVersion.length() - 1) + paramPath ;
            boolQueryBuilder
                    .filter()
                    .add(QueryBuilders.termsQuery("versions", versions));
        }

        //9、专题检索（多选，前缀模糊匹配） 用filter
        if (queryWikiPageVO.getTopics() != null && queryWikiPageVO.getTopics().size() > 0) {
            String tmpTopicCodes = "";
            wiki.setTopics(KmStringUtils.concatListToString(queryWikiPageVO.getTopics()));
            BoolQueryBuilder boolQueryBuilderTopicCodes = QueryBuilders.boolQuery();
            List<String> topicCodes = queryWikiPageVO.getTopics();
            for (int i = 0; i < topicCodes.size(); i++) {
                //模糊匹配，匹配某个字符串开头的记录prefixQuery
                boolQueryBuilderTopicCodes
                        .should()
                        .add(QueryBuilders.prefixQuery("topicCodes", topicCodes.get(i)));

                tmpTopicCodes = tmpTopicCodes.concat(topicCodes.get(i));

                tmpTopicCodes = tmpTopicCodes.concat(",");
            }
            paramPath = SearchTermSplit + "专题: " + tmpTopicCodes.substring(0, tmpTopicCodes.length() - 1) + paramPath ;

            boolQueryBuilder
                    .filter()
                    .add(boolQueryBuilderTopicCodes);
            return boolQueryBuilder;
        }

        //10、文号，（前缀模糊匹配） 用filter
        if(queryWikiPageVO.getFileNo() != null && !queryWikiPageVO.getFileNo().isEmpty()){
            paramPath = SearchTermSplit + "文号: " + queryWikiPageVO.getFileNo() + paramPath  ;
            boolQueryBuilder
                    .filter()
                    .add(QueryBuilders.wildcardQuery("fileNo", queryWikiPageVO.getFileNo()));
        }

        //去掉检索参数第一个+
        if(!paramPath.isEmpty() && paramPath.length()>3){
            paramPath = paramPath.substring(3);
        }


        //11、发布状态必须为1
       /* boolQueryBuilder
                .filter()
                .add(QueryBuilders.termQuery("releaseFlag",1));*/
        return boolQueryBuilder;
    }


}
