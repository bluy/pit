package com.xfcode.mapper;


import com.xfcode.domain.Wiki;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

@Repository
public interface WikiMapper extends BaseMapper<Wiki> {

}
