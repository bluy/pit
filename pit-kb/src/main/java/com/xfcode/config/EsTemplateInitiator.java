package com.xfcode.config;

import com.xfcode.service.EsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@Order(1) //指定顺序
public class EsTemplateInitiator implements CommandLineRunner {
    @Autowired
    private EsService esService;
    @Override
    public void run(String... args)  {
        esService.initWikiTemplate();
    }
}
