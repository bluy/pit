<p align="center">
	<img alt="logo" src="https://gitee.com/abocode-source/pit/raw/master/doc/images/logo.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">pit v3.0.0</h1>
<h4 align="center">pit-开源的标准化企业级系统开发平台</h4>

### 平台简介
pit是居于Java开发的一套开源的企业级系统开发平台,其核心设计目标是让系统开发标准化、知识产权自主可控。具备学习简单、易扩展、易维护的特点。帮企业快速构建高质量的业务系统。

* 移动端采用Vue、Uniapp、Uview。
* PC端采用Vue、Element UI。
* 后端采用Spring Boot、Mybatis、Spring Security、Redis & Jwt。
* 使用Flowable、Bpmn.io实现工作流。
* 使用Websocket实现即时通讯。
* 使用OSS、COS实现对象存储。
* 使用Handsontable实现类Excel拖拽赋值的Web数据录入。
* 使用Vxe-table实现单行编辑，即时保存效果。
* 使用ECharts,UCharts实现数据可视化图表。
* 使用DataV展示可视化大屏数据。
* 使用IReport实现企业级Web报表。
* 使用kkFileView实现在线预览，支持doc,docx,Excel,pdf,txt,zip,rar,图片等。
* 使用OAuth2实现三方应用授权。
* 支持多种登录方式（扫码登录，验证码登录，密码登录）。
* 支持微信、支付宝等三方支付。
* 支持加载动态权限菜单，控制菜单权限，按钮权限，数据权限。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 支持gpt，将gpt应用到项目中，方便用户编写总结报告、图片、视频等操作。


### 内置系统功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  数据管理：配置系统用户的数据权限，如只能查看自己的数据，查看本部门的数据，查看所有数据。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 缓存监控：对系统的缓存信息查询，命令统计等。
17. 在线构建器：拖动表单元素生成相应的HTML代码。
18. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。

# 前端地址

* 移动版

https://gitee.com/xfcode-source/pit-uniapp

* pc版本

https://gitee.com/xfcode-source/pit-web

### QQ交流群
|  2群(140586555) | 
| :------: | 
| <img src="https://gitee.com/abocode-source/pit/raw/master/doc/images/qq01.jpg" width="200px">|

### 结语

欢迎一起探讨，如果您觉得还可以，您可以给我点一个star
