package com.xfcode.common;

import com.xfcode.common.core.controller.BaseController;

public class BController extends BaseController {

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected R<Void> toAjaxR(int rows) {
        return rows > 0 ? R.ok() : R.fail();
    }

    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected R<Void> toAjaxR(boolean result) {
        return result ? R.ok() : R.fail();
    }
}
