package com.xfcode.workflow.api.dto;

import lombok.Builder;
import lombok.Data;


/**
 * @author KonBAI
 * @createTime 2022/3/10 00:12
 */
@Data
@Builder
public class WfCommentDto {

    /**
     * 意见类别 0 正常意见  1 退回意见 2 驳回意见
     */
    private String type;

    /**
     * 意见内容
     */
    private String comment;
}
