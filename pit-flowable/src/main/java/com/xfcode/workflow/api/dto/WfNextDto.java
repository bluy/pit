package com.xfcode.workflow.api.dto;

import com.xfcode.common.core.domain.entity.SysRole;
import com.xfcode.common.core.domain.entity.SysUser;
import lombok.Data;

import java.util.List;

/**
 * 动态人员、组
 * @author KonBAI
 * @createTime 2022/3/10 00:12
 */
@Data
public class WfNextDto {

    private String type;

    private String vars;

    private List<SysUser> userList;

    private List<SysRole> roleList;
}
