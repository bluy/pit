package com.xfcode.workflow.service;

import com.xfcode.common.PageQuery;
import com.xfcode.common.core.page.TableDataInfo;
import com.xfcode.flowable.core.domain.ProcessQuery;
import com.xfcode.workflow.api.vo.WfDeployVo;

import java.util.List;

/**
 * @author KonBAI
 * @createTime 2022/6/30 9:03
 */
public interface IWfDeployService {

    TableDataInfo<WfDeployVo> queryPageList(ProcessQuery processQuery, PageQuery pageQuery);

    TableDataInfo<WfDeployVo> queryPublishList(String processKey, PageQuery pageQuery);

    void updateState(String definitionId, String stateCode);

    String queryBpmnXmlById(String definitionId);

    void deleteByIds(List<String> deployIds);
}
