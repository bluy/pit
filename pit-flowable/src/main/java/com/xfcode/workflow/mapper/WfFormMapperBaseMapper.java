package com.xfcode.workflow.mapper;

import com.xfcode.common.mybatiesplus.mapper.MybatiesPlusBaseMapper;
import com.xfcode.workflow.domain.WfForm;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import com.xfcode.workflow.api.vo.WfFormVo;

import java.util.List;

/**
 * 流程表单Mapper接口
 *
 * @author KonBAI
 * @createTime 2022/3/7 22:07
 */
@Repository
public interface WfFormMapperBaseMapper extends MybatiesPlusBaseMapper<WfFormMapperBaseMapper, WfForm, WfFormVo> {

    List<WfFormVo> selectFormVoList(@Param(Constants.WRAPPER) Wrapper<WfForm> queryWrapper);
}
