package com.xfcode.workflow.mapper;

import com.xfcode.common.mybatiesplus.mapper.MybatiesPlusBaseMapper;
import com.xfcode.workflow.domain.WfCopy;
import org.springframework.stereotype.Repository;
import com.xfcode.workflow.api.vo.*;
/**
 * 流程抄送Mapper接口
 *
 * @author KonBAI
 * @date 2022-05-19
 */
@Repository
public interface WfCopyMapperBaseMapper extends MybatiesPlusBaseMapper<WfCopyMapperBaseMapper, WfCopy, WfCopyVo> {

}
