package com.xfcode.workflow.mapper;

import com.xfcode.common.mybatiesplus.mapper.MybatiesPlusBaseMapper;
import com.xfcode.workflow.api.vo.WfCategoryVo;
import com.xfcode.workflow.domain.WfCategory;
import org.springframework.stereotype.Repository;

/**
 * 流程分类Mapper接口
 *
 * @author KonBAI
 * @date 2022-01-15
 */
@Repository
public interface WfCategoryMapperBaseMapper extends MybatiesPlusBaseMapper<WfCategoryMapperBaseMapper, WfCategory, WfCategoryVo> {

}
