package com.xfcode.workflow.mapper;

import com.xfcode.common.mybatiesplus.mapper.MybatiesPlusBaseMapper;
import com.xfcode.workflow.domain.WfDeployForm;
import org.springframework.stereotype.Repository;
import com.xfcode.workflow.api.vo.*;
/**
 * 流程实例关联表单Mapper接口
 *
 * @author KonBAI
 * @createTime 2022/3/7 22:07
 */
@Repository
public interface WfDeployFormMapperBaseMapper extends MybatiesPlusBaseMapper<WfDeployFormMapperBaseMapper, WfDeployForm, WfDeployFormVo> {

}
