package cn.moonshot.platform.util.bean;


import java.util.List;

public class ChatCompletionFileRequest extends ChatCompletionRequest{
  private   List<String> refs;
  private  boolean use_search;
  private  String  kimiplus_id="kimi";
  private  boolean is_pro_search;
    public ChatCompletionFileRequest(String model, List<ChatCompletionMessage> messages, int maxTokens, float temperature, int n) {
        super(model, messages, maxTokens, temperature, n);
    }

    public ChatCompletionFileRequest(String model, List<ChatCompletionMessage> messages, int maxTokens, float temperature, int n, List<String> refs) {
        super(model, messages, maxTokens, temperature, n);
        this.refs = refs;
    }

    public List<String> getRefs() {
        return refs;
    }

    public void setRefs(List<String> refs) {
        this.refs = refs;
    }

    public boolean isUse_search() {
        return use_search;
    }

    public void setUse_search(boolean use_search) {
        this.use_search = use_search;
    }

    public String getKimiplus_id() {
        return kimiplus_id;
    }

    public void setKimiplus_id(String kimiplus_id) {
        this.kimiplus_id = kimiplus_id;
    }

    public boolean isIs_pro_search() {
        return is_pro_search;
    }

    public void setIs_pro_search(boolean is_pro_search) {
        this.is_pro_search = is_pro_search;
    }
}
