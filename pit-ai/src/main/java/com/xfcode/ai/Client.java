package com.xfcode.ai;

import cn.moonshot.platform.util.MoonClient;
import cn.moonshot.platform.util.bean.ChatCompletionMessage;
import cn.moonshot.platform.util.bean.ChatCompletionRequest;
import cn.moonshot.platform.util.bean.ChatMessageRole;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Client {
    private String appKey;
    private GptType gptType;

    private MoonClient moonClient;
    public Client(String appKey) {
        this.appKey = appKey;
        this.gptType = GptType.KIMI;
        this.moonClient = new MoonClient(appKey);
    }
    public Client(String appKey, GptType gptType) {
        this.appKey = appKey;
        this.gptType = gptType;
        this.moonClient = new MoonClient(appKey);
    }
    public MoonClient getMoonClient() {
        return moonClient;
    }

    public String getAnswer(String template, String input) {

        List<ChatCompletionMessage> messages=new ArrayList<>();
        messages.add(new ChatCompletionMessage(ChatMessageRole.SYSTEM.value(),template));
        messages.add(new ChatCompletionMessage(ChatMessageRole.USER.value(), input));
        ChatCompletionRequest chatCompletionRequest     = new ChatCompletionRequest(
                "moonshot-v1-8k",
                messages,
                200,
                0.3f,
                1
        );
        try {
          return getMoonClient().chatCompletion(chatCompletionRequest).getChoices().get(0).getMessage().getContent();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
