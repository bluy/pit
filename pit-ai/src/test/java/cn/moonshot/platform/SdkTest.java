package cn.moonshot.platform;

import cn.moonshot.platform.util.MoonClient;
import cn.moonshot.platform.util.bean.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SdkTest extends BaseTest {
    @Test
  public  void testChat() {
       /* String apiKey = System.getenv("MOONSHOT_API_KEY");
        if (apiKey == null) {
            System.out.println("Please set MOONSHOT_API_KEY env");
            return;
        }*/
        MoonClient moonClient = new MoonClient(apiKey);
        try {
            ModelsList models = moonClient.getModels();
            for (Model model : models.getData()) {
                System.out.println(model.getId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<ChatCompletionMessage> messages=new ArrayList<>();
        messages.add(new ChatCompletionMessage(ChatMessageRole.SYSTEM.value(),
                "你是 Kimi，由 Moonshot AI 提供的人工智能助手，你更擅长中文和英文的对话。你会为用户提供安全，有帮助，准确的回答。同时，你会拒绝一些涉及恐怖主义，种族歧视，黄色暴力等问题的回答。Moonshot AI 为专有名词，不可翻译成其他语言。"));
        messages.add(new ChatCompletionMessage(ChatMessageRole.USER.value(),
                "你好，我叫李雷，1+1等于多少？"));

        try {
            moonClient.chatCompletionStream(new ChatCompletionRequest(
                    "moonshot-v1-8k",
                    messages,
                    50,
                    0.3f,
                    1
            )).subscribe(
                    streamResponse -> {
                        if (streamResponse.getChoices().isEmpty()) {
                            return;
                        }
                        for (ChatCompletionStreamChoice choice : streamResponse.getChoices()) {
                            String finishReason = choice.getFinishReason();
                            if (finishReason != null) {
                                System.out.println("finish reason: " + finishReason);
                                continue;
                            }
                            System.out.println(choice.getDelta().getContent());
                        }
                    },
                    error -> {
                        error.printStackTrace();
                    },
                    () -> {
                        System.out.println("complete");
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
