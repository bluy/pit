package cn.moonshot.platform;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.moonshot.platform.util.Message;
import cn.moonshot.platform.util.MoonClient;
import cn.moonshot.platform.util.RoleEnum;
import cn.moonshot.platform.util.bean.Model;
import cn.moonshot.platform.util.bean.ModelsList;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;


public class ApiTest extends BaseTest {

    @Test
    void getModelList() {
        System.out.println(getPlatformClient().getModelList());
    }
    @Test
    void getModels() {
        MoonClient moonClient = getMoonClient();
        try {
            ModelsList models = moonClient.getModels();
            for (Model model : models.getData()) {
                System.out.println(model.getId());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    void uploadFile() {
        System.out.println(getPlatformClient().uploadFile(FileUtil.file("E:\\WorkSpace\\opensource\\pit\\pit-ai\\src\\test\\resources\\file\\form.png")));
    }

    @Test
    void uploadFile2() {
        System.out.println(getPlatformClient().uploadFile(FileUtil.file("E:\\WorkSpace\\opensource\\pit\\pit-ai\\src\\test\\resources\\file\\form.jpg")));
    }

    @Test
    void getFileList() {
        System.out.println(getPlatformClient().getFileList());
    }

    @Test
    void deleteFile() {
//        System.out.println(MoonshotAiUtils.deleteFile("co17orilnl9coc91noh0"));
        System.out.println(getPlatformClient().getFileList());
    }

    @Test
    void getFileContent() {
        System.out.println(getPlatformClient().getFileContent("cq7ugqm0atpahkf4agjg"));
    }

    @Test
    void getFileDetail() {
        System.out.println(getPlatformClient().getFileDetail("cq7ugqm0atpahkf4agjg"));
    }

    @Test
    void estimateTokenCount() {
        List<Message> messages = CollUtil.newArrayList(
                new Message(RoleEnum.system.name(), "你是kimi AI"),
                new Message(RoleEnum.user.name(), "hello")
        );
        System.out.println(getPlatformClient().estimateTokenCount("moonshot-v1-8k", messages));
    }

    @Test
    void chat(){
        List<Message> messages = CollUtil.newArrayList(
                new Message(RoleEnum.system.name(), "你是kimi AI"),
                new Message(RoleEnum.user.name(), "hello")
        );
        getPlatformClient().chat("moonshot-v1-8k",messages);
    }

}
