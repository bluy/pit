package cn.moonshot.platform.file;
import cn.moonshot.platform.BaseTest;
import cn.moonshot.platform.util.PlatformClient;
import org.junit.jupiter.api.Test;
import org.springframework.data.repository.init.ResourceReader;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

public class FileTest extends BaseTest {
    @Test
    void redFile() {
        // 假设我们要读取的文件名为"example.txt"
        String resourceName = "file/form.png";
        System.out.println();
        System.out.println(getPlatformClient().uploadFile(getFile(resourceName)));
    }

    @Test
    void upFile() {
        // 假设我们要读取的文件名为"example.txt"
        String resourceName = "file/b.pdf";
        System.out.println(getPlatformClient().uploadFile(getFile(resourceName)));
    }

    public File getFile(String resourceName) {
        // 获取当前类的ClassLoader
        ClassLoader classLoader = ResourceReader.class.getClassLoader();
        // 通过ClassLoader获取资源的URL
        URL resource = classLoader.getResource(resourceName);

        // 检查资源是否存在
        if (resource == null) {
            System.out.println("资源文件不存在: " + resourceName);
            return null;
        }
        try {
            return  new File(resource.toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

}
