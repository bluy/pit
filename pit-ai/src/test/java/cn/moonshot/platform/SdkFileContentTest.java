package cn.moonshot.platform;

import cn.moonshot.platform.util.MoonClient;
import cn.moonshot.platform.util.PlatformClient;
import cn.moonshot.platform.util.bean.*;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SdkFileContentTest  extends BaseTest {

    @Test
    public  void testChat() throws InterruptedException {

        //pdf
//        String content = MoonshotAiUtils.getFileContent("cq7va460atpahkf4b24g");
        //图片
        String content = getPlatformClient().getFileContent("cq7vlrqtnn0t8799jds0");
        ChatCompletionMessage contentMessage = new Gson().fromJson(content, ChatCompletionMessage.class);


        List<ChatCompletionMessage> messages=new ArrayList<>();
        messages.add(new ChatCompletionMessage(ChatMessageRole.SYSTEM.value(),
                contentMessage.getContent()));
       messages.add(new ChatCompletionMessage(ChatMessageRole.USER.value(),
               "请转换为JSON"));
        StringBuilder sb = new StringBuilder("resp:");
        try {
            ChatCompletionRequest chatCompletionRequest     = new ChatCompletionRequest(
                    "moonshot-v1-8k",
                    messages,
                    200,
                    0.3f,
                    1
            );

//            chatCompletionRequest.setRefs(Arrays.asList("cq7ugqm0atpahkf4agjg"));
            getMoonClient().chatCompletionStream(chatCompletionRequest).subscribe(
                    streamResponse -> {
                        if (streamResponse.getChoices().isEmpty()) {
                            return;
                        }
                        for (ChatCompletionStreamChoice choice : streamResponse.getChoices()) {
                            String finishReason = choice.getFinishReason();
                            if (finishReason != null) {
                                System.out.println("finish reason: " + finishReason);
                                continue;
                            }
                            System.out.println(choice.getDelta().getContent());
                            sb.append(choice.getDelta().getContent());
                        }
                    },
                    error -> {
                        error.printStackTrace();
                    },
                    () -> {
                        System.out.println("complete");
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(sb);
        Thread.sleep(100000);
    }
}
