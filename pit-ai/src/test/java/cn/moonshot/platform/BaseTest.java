package cn.moonshot.platform;

import cn.moonshot.platform.util.MoonClient;
import cn.moonshot.platform.util.PlatformClient;

public class BaseTest {
    /***
     * 请更换为自己的key
     */
    public  String apiKey="";

    public PlatformClient getPlatformClient() {
        return new MoonClient(apiKey).getPlatformClient();
    }
    public MoonClient getMoonClient() {
        return new MoonClient(apiKey);
    }
}
