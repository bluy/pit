package cn.moonshot.platform;

import cn.moonshot.platform.util.bean.*;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SdkFileTest extends BaseTest {

    @Test
    public  void testChat() {
        List<ChatCompletionMessage> messages=new ArrayList<>();
       messages.add(new ChatCompletionMessage(ChatMessageRole.USER.value(),
                "转换成json格式"));
        try {
            ChatCompletionFileRequest chatCompletionRequest     = new ChatCompletionFileRequest(
                    "moonshot-v1-8k",
                    messages,
                    50,
                    0.3f,
                    1
            );
            chatCompletionRequest.setRefs(Arrays.asList("cq7ugqm0atpahkf4agjg"));
            getMoonClient().chatCompletionStream(chatCompletionRequest).subscribe(
                    streamResponse -> {
                        if (streamResponse.getChoices().isEmpty()) {
                            return;
                        }
                        for (ChatCompletionStreamChoice choice : streamResponse.getChoices()) {
                            String finishReason = choice.getFinishReason();
                            if (finishReason != null) {
                                System.out.println("finish reason: " + finishReason);
                                continue;
                            }
                            System.out.println(choice.getDelta().getContent());
                        }
                    },
                    error -> {
                        error.printStackTrace();
                    },
                    () -> {
                        System.out.println("complete");
                    }
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
